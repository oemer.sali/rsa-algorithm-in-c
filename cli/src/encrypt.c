/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rsa/rsa.h>

int main(int argc, char *argv[])
{
	// Read given arguments
	char *input_name = NULL;
	char *key_name = NULL;
	char *output_name = NULL;

	for (int i = 1; i < argc;)
	{
		if (strcmp(argv[i], "-i") == 0)
		{
			++i;
			if (i == argc)
			{
				fprintf(stderr, "Error: Missing input filename\n");
				exit(EXIT_FAILURE);
			}
			else
			{
				char *extension = strrchr(argv[i], '.');
				if (extension == NULL || strcmp(extension, ".plain") != 0)
				{
					fprintf(stderr, "Error: Input file is not a plaintext file\n");
					exit(EXIT_FAILURE);
				}
				else
				{
					size_t length = strlen(argv[i]);
					input_name = malloc(length+1);
					input_name = strcpy(input_name, argv[i]);
					output_name = malloc(length+2);
					output_name = strncpy(output_name, argv[i], length-5);
					output_name[length-5] = '\0';
					output_name = strcat(output_name, "cipher");
					++i;
				}
			}
		}
		else if (strcmp(argv[i], "-k") == 0)
		{
			++i;
			if (i == argc)
			{
				fprintf(stderr, "Error: Missing key filename\n");
				exit(EXIT_FAILURE);
			}
			else
			{
				char *extension = strrchr(argv[i], '.');
				if (extension == NULL || strcmp(extension, ".public") != 0)
				{
					fprintf(stderr, "Error: Key file is not a public key file\n");
					exit(EXIT_FAILURE);
				}
				else
				{
					key_name = malloc(strlen(argv[i])+1);
					key_name = strcpy(key_name, argv[i]);
					++i;
				}
			}
		}
		else
		{
			fprintf(stderr, "Error: Unknown argument '%s'\n", argv[i]);
			exit(EXIT_FAILURE);
		}
	}

	if (input_name == NULL)
	{
		fprintf(stderr, "Error: No input file specified\n");
		exit(EXIT_FAILURE);
	}
	if (key_name == NULL)
	{
		fprintf(stderr, "Error: No key file specified\n");
		exit(EXIT_FAILURE);
	}


	// Read rsa key from key file
	printf("Reading public key from '%s'\n", key_name);
	rsa_key_t *public_key = rsa_key_create();
	FILE *key_file = fopen(key_name, "r");
	size_t length;
	fscanf(key_file, "%zu", &length);
	char *buffer = malloc(length+1);
	fscanf(key_file, "%s", buffer);
	public_key->exponent = natural_from_string(buffer);
	fscanf(key_file, "%s", buffer);
	public_key->modulus= natural_from_string(buffer);
	free(buffer);
	fclose(key_file);

	// Encrypt input file
	printf("Encrypting input file '%s' and writing it to '%s'\n", input_name, output_name);
	FILE *input_file = fopen(input_name, "r");
	if (input_file == NULL)
	{
		fprintf(stderr, "Error: Can not open input file '%s'\n", input_name);
		exit(EXIT_FAILURE);
	}
	FILE *output_file = fopen(output_name, "w");
	if (output_file == NULL)
	{
		fprintf(stderr, "Error: Can not open output file '%s'\n", output_name);
		exit(EXIT_FAILURE);
	}
	int c;
	while ((c = fgetc(input_file)) != EOF)
	{
		natural_t *x = natural_from_uint(c);
		natural_t *y = natural_from_uint(0);
		rsa_transform(x, public_key, y);
		char *s = natural_to_string(y);
		fprintf(output_file, "%s\n", s);
		free(s);
		natural_destroy(y);
		natural_destroy(x);
	}
	fclose(output_file);
	fclose(input_file);
	rsa_key_destroy(public_key);

	exit(EXIT_SUCCESS);
}
