/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rsa/rsa.h>

int main(int argc, char *argv[])
{
	size_t bits = 0;
	char *public_name = NULL;
	char *private_name = NULL;

	// Read given arguments
	for (int i = 1; i < argc;)
	{
		if (strcmp(argv[i], "-k") == 0)
		{
			++i;
			if (i == argc)
			{
				fprintf(stderr, "Error: Missing key filename\n");
				exit(EXIT_FAILURE);
			}
			else
			{
				public_name = malloc(strlen(argv[i])+8);
				public_name = strcpy(public_name, argv[i]);
				public_name = strcat(public_name, ".public");
				private_name = malloc(strlen(argv[i])+9);
				private_name = strcpy(private_name, argv[i]);
				private_name = strcat(private_name, ".private");
				++i;
			}
		}
		else if (strcmp(argv[i], "-b") == 0)
		{
			++i;
			if (i == argc)
			{
				fprintf(stderr, "Error: Missing size argument\n");
				exit(EXIT_FAILURE);
			}
			else if (sscanf(argv[i], "%zu", &bits) == 0)
			{
				fprintf(stderr, "Error: The given size '%s' must be a natural number\n", argv[i]);
				exit(EXIT_FAILURE);
			}
			else
				++i;
		}
		else
		{
			fprintf(stderr, "Error: Unknown argument '%s'\n", argv[i]);
			exit(EXIT_FAILURE);
		}
	}

	// Generate RSA keys
	printf("Generating RSA keys with %zu bits\n", bits);
	rsa_key_t *public_key, *private_key;
	public_key = rsa_key_create();
	private_key = rsa_key_create();
	rsa_generate_keys(bits, public_key, private_key);

	// Write RSA keys to files
	printf("Writing public key to '%s'\n", public_name);
	FILE *public_file = fopen(public_name, "w");
	if (public_file == NULL)
	{
		fprintf(stderr, "Error: Can not open file '%s'", public_name);
		exit(EXIT_FAILURE);
	}
	else
	{
		char *e = natural_to_string(public_key->exponent);
		char *n = natural_to_string(public_key->modulus);
		fprintf(public_file, "%zu\n%s\n%s", strlen(n), e, n);
		free(n);
		free(e);
	}
	fclose(public_file);
	free(public_name);
	rsa_key_destroy(public_key);

	printf("Writing private key to '%s'\n", private_name);
	FILE *private_file = fopen(private_name, "w");
	if (private_file == NULL)
	{
		fprintf(stderr, "Error: Can not open file '%s'", private_name);
		exit(EXIT_FAILURE);
	}
	else
	{
		char *d = natural_to_string(private_key->exponent);
		char *n = natural_to_string(private_key->modulus);
		fprintf(private_file, "%zu\n%s\n%s", strlen(n), d, n);
		free(n);
		free(d);
	}
	fclose(private_file);
	free(private_name);
	rsa_key_destroy(private_key);

	exit(EXIT_SUCCESS);
}
