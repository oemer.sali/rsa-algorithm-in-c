/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rsa/rsa.h>

int main(int argc, char *argv[])
{
	// Read given arguments
	char *input_name = NULL;
	char *key_name = NULL;
	char *output_name = NULL;

	for (int i = 1; i < argc;)
	{
		if (strcmp(argv[i], "-i") == 0)
		{
			++i;
			if (i == argc)
			{
				fprintf(stderr, "Error: Missing input filename\n");
				exit(EXIT_FAILURE);
			}
			else
			{
				char *extension = strrchr(argv[i], '.');
				if (extension == NULL || strcmp(extension, ".cipher") != 0)
				{
					fprintf(stderr, "Error: Input file is not a ciphertext file\n");
					exit(EXIT_FAILURE);
				}
				else
				{
					size_t length = strlen(argv[i]);
					input_name = malloc(length+1);
					input_name = strcpy(input_name, argv[i]);
					output_name = malloc(length);
					output_name = strncpy(output_name, argv[i], length-6);
					output_name[length-6] = '\0';
					output_name = strcat(output_name, "plain");
					++i;
				}
			}
		}
		else if (strcmp(argv[i], "-k") == 0)
		{
			++i;
			if (i == argc)
			{
				fprintf(stderr, "Error: Missing key filename\n");
				exit(EXIT_FAILURE);
			}
			else
			{
				char *extension = strrchr(argv[i], '.');
				if (extension == NULL || strcmp(extension, ".private") != 0)
				{
					fprintf(stderr, "Error: Key file is not a private key file\n");
					exit(EXIT_FAILURE);
				}
				else
				{
					key_name = malloc(strlen(argv[i])+1);
					key_name = strcpy(key_name, argv[i]);
					++i;
				}
			}
		}
		else
		{
			fprintf(stderr, "Error: Unknown argument '%s'\n", argv[i]);
			exit(EXIT_FAILURE);
		}
	}

	if (input_name == NULL)
	{
		fprintf(stderr, "Error: No input file specified\n");
		exit(EXIT_FAILURE);
	}
	if (key_name == NULL)
	{
		fprintf(stderr, "Error: No key file specified\n");
		exit(EXIT_FAILURE);
	}

	// Read rsa key from file
	printf("Reading private key from '%s'\n", key_name);
	rsa_key_t *private_key = rsa_key_create();
	FILE *key_file = fopen(key_name, "r");
	size_t length;
	fscanf(key_file, "%zu", &length);
	char *s = malloc(length+1);
	fscanf(key_file, "%s", s);
	private_key->exponent = natural_from_string(s);
	fscanf(key_file, "%s", s);
	private_key->modulus= natural_from_string(s);
	fclose(key_file);

	// Decrypt input file
	printf("Decrypting input file '%s' and writing it to '%s'\n", input_name, output_name);
	FILE *input_file = fopen(input_name, "r");
	if (input_file == NULL)
	{
		fprintf(stderr, "Error: Can not open input file '%s'\n", input_name);
		exit(EXIT_FAILURE);
	}
	FILE *output_file = fopen(output_name, "w");
	if (output_file == NULL)
	{
		fprintf(stderr, "Error: Can not open output file '%s'\n", output_name);
		exit(EXIT_FAILURE);
	}
	while ((s = fgets(s, length+2, input_file)) != NULL)
	{
		s[strlen(s)-1] = '\0';
		natural_t *x = natural_from_string(s);
		natural_t *y = natural_from_uint(0);
		rsa_transform(x, private_key, y);
		int c = natural_to_uint(y);
		fprintf(output_file, "%c", c);
		natural_destroy(y);
		natural_destroy(x);
	}
	free(s);
	fclose(output_file);
	fclose(input_file);
	rsa_key_destroy(private_key);

	exit(EXIT_SUCCESS);
}
