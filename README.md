# RSA Algorithm in C
This is an implementation of the [RSA cryptosystem](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) in pure C that supports arbitrary precision integers for key generation without any dependence on third-party libraries. It consists of

1. the self-contained RSA library under `rsa` that implements the core functionality for key generation and encryption/decryption.

2. a simple reference implementation of command-line programs under `cli` that demonstrate the usage of the library for encryption and decryption of plain text files.

Note that this project is intended for teaching purposes only, since the pseudo-random number generator is not cryptographically secure!

## The RSA library
In **`natural`** we define arbitrary precision unsigned integers as arrays of single digits and corresponding arithmetic operations (addition, subtraction,...) on top of them. The underlying digit data type is `uintmax_t` and thus expands to the maximum fixed width unsigned integer type already provided by your system. This increases the performance of the arithmetic operations that make use of the corresponding built-in operations on digits wherever possible. The internal array of digits is dynamically expanded and shrinked to tightly fit the represented number.

In **`rsa`** a public or private key is defined by its exponent and modulus of arbitrary precision. To choose the prime factors for key generation, we randomly choose integers and perform multiple iterations of the [Miller–Rabin primality test](https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test). All needed algorithms (modular exponentiation and inversion, Miller-Rabin test) are implemented using our arbitrary precision unsigned integers. This allows the user to choose the desired bit-length of the RSA-modulus, encryption and decryption exponents during the key generation.

## Command-line programs
To demonstrate the functionality of the RSA library, we provide the following command-line programs that encrypt and decrypt plain text files character by character:

**`keygen`**: This program generates a new pair of RSA keys and stores them in text files with the extensions `.public` and `.private`, respectively. The desired bit-length of the RSA-modulus must be set with the flag `-b`. A key filename must be provided with the flag `-k`. Example:
```bash
$ keygen -b 512 -k key
Generating RSA keys with 512 bits
Writing public key to 'key.public'
Writing private key to 'key.private'
```

**`encrypt`**: To encrypt a given plaintext file, it must be saved with the extension `.plain` and provided to the program `encrypt` with the flag `-i`. The public key to use for encryption must be set with the flag `-k`. The resulting ciphertext is saved under the given input filename with the extension `.cipher`. Example:
```bash
$ echo "Hello World!!!">file.plain
$ encrypt -i file.plain -k key.public
Reading public key from 'key.public'
Encrypting input file 'file.plain' and writing it to 'file.cipher'
```

**`decrypt`**: To decrypt a given ciphertext file with the extension `.cipher` back again, it must be provided to the program `decrypt` with the flag `-i`. The private key to use for decryption must be set with the flag `-k`. The resulting plaintext is saved under the given input filename with the extension `.plain`. Example:

```bash
$ mv file.plain file.backup.plain
$ decrypt -i file.cipher -k key.private
Reading private key from 'key.private'
Decrypting input file 'file.cipher' and writing it to 'file.plain'
$ diff -s file.plain file.backup.plain
Files file.plain and file.backup.plain are identical
```

## Build, test and install the project
You can build the entire project out of source by using cmake and make via:
```bash
$ mkdir build
$ cd build
$ cmake ..
$ make all
```

To run the above command-line program examples, you can simply invoke the test target of the Makefile:
```bash
$ make test
```

The shared library `librsa.so` and programs `keygen`, `encrypt` and `decrypt` can be installed to `/usr/local/{bin|lib}` via:
```bash
$ make install
$ ldconfig
```
