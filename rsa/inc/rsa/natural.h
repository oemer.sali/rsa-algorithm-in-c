/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifndef NATURAL_H
#define NATURAL_H

#include <stddef.h>
#include <stdint.h>

// Type definitions
typedef uintmax_t uint_t;

typedef uintmax_t digit_t;

typedef enum
{
	LESS = -1,
	EQUAL = 0,
	GREATER = 1
} comparison_t;

typedef enum
{
	EVEN = 0,
	ODD = 1
} parity_t;

typedef struct
{
	size_t size;
	digit_t *digits;
} natural_t;

// Constructors/Destructors
natural_t *natural_from_uint(const uint_t n);
uint_t natural_to_uint(const natural_t *x);
natural_t *natural_from_string(const char *s);
char *natural_to_string(const natural_t *x);
natural_t *natural_random(const size_t b);
void natural_destroy(natural_t *x);

// Arithmetic operations
void natural_assign(const natural_t *x, natural_t *y);
void natural_add(const natural_t *x, const natural_t *y, natural_t *z);
void natural_subtract(const natural_t *x, const natural_t *y, natural_t *z);
void natural_multiply(const natural_t *x, const natural_t *y, natural_t *z);
void natural_divide(const natural_t *x, const natural_t *y, natural_t *z);
void natural_modulo(const natural_t *x, const natural_t *y, natural_t *z);

// Logical operations
comparison_t natural_compare(const natural_t *x, const natural_t *y);
parity_t natural_parity(const natural_t *x);

#endif // NATURAL_H
