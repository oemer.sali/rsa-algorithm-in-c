/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#ifndef RSA_H
#define RSA_H

#include <stddef.h>
#include <rsa/natural.h>

// Type definitions
typedef struct
{
	natural_t *exponent;
	natural_t *modulus;
} rsa_key_t;

// Constructors/Destructors
rsa_key_t *rsa_key_create();
void rsa_key_destroy(rsa_key_t *key);

// Operations
void rsa_generate_keys(const size_t bits, rsa_key_t *public_key, rsa_key_t *private_key);
void rsa_transform(const natural_t *input, const rsa_key_t *key, natural_t *output);

#endif // RSA_H
