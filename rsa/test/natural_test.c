#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <rsa/natural.h>

void natural_uint_test(const size_t i)
{
	natural_t *x;
	uint_t n_gt, n_p;
	if (i == 0)
		n_gt = 0;
	else if (i == 1)
		n_gt = 65536;
	x = natural_from_uint(n_gt);
	n_p = natural_to_uint(x);
	assert(n_gt == n_p);
	natural_destroy(x);
}

void natural_string_test(const size_t i)
{
	natural_t *x;
	char *s_gt, *s_p;
	if (i == 0)
		s_gt = "0";
	else if (i == 1)
		s_gt = "65536";
	else if (i == 2)
		s_gt = "534436403405035981305983960138503953050319850319";
	x = natural_from_string(s_gt);
	s_p = natural_to_string(x);
	assert(strcmp(s_gt, s_p) == 0);
	free(s_p);
	natural_destroy(x);
}

void natural_add_test(const size_t i)
{
	natural_t *x, *y, *z_gt, *z_p;
	if (i == 0)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("382347902387403287431784038749031874503874392847");
		z_gt = natural_from_string("916784305792439268737767998887535827554194243166");
	}
	else if (i == 1)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("0");
		z_gt = natural_from_string("652421309814098120348912049830582304982304921309");
	}
	else if (i == 2)
	{
		x = natural_from_string("0");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("652421309814098120348912049830582304982304921309");
	}
	z_p = natural_from_uint(0);
	natural_add(x, y, z_p);
	assert(natural_compare(z_gt, z_p) == EQUAL);
	natural_destroy(z_p);
	natural_destroy(z_gt);
	natural_destroy(y);
	natural_destroy(x);
}

void natural_subtract_test(const size_t i)
{
	natural_t *x, *y, *z_gt, *z_p;
	if (i == 0)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("382347902387403287431784038749031874503874392847");
		z_gt = natural_from_string("152088501017632693874199921389472078546445457472");
	}
	else if (i == 1)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("0");
		z_gt = natural_from_string("652421309814098120348912049830582304982304921309");
	}
	else if (i == 2)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("0");
	}
	z_p = natural_from_uint(0);
	natural_subtract(x, y, z_p);
	assert(natural_compare(z_gt, z_p) == EQUAL);
	natural_destroy(z_p);
	natural_destroy(z_gt);
	natural_destroy(y);
	natural_destroy(x);
}

void natural_multiply_test(const size_t i)
{
	natural_t *x, *y, *z_gt, *z_p;
	if (i == 0)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("382347902387403287431784038749031874503874392847");
		z_gt = natural_from_string("204340637801383583289184227432550641383602870140542544859184546267691831275979376024638044268193");
	}
	else if (i == 1)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("1");
		z_gt = natural_from_string("652421309814098120348912049830582304982304921309");
	}
	else if (i == 2)
	{
		x = natural_from_string("1");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("652421309814098120348912049830582304982304921309");
	}
	else if (i == 3)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("0");
		z_gt = natural_from_string("0");
	}
	else if (i == 4)
	{
		x = natural_from_string("0");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("0");
	}
	z_p = natural_from_uint(0);
	natural_multiply(x, y, z_p);
	assert(natural_compare(z_gt, z_p) == EQUAL);
	natural_destroy(z_p);
	natural_destroy(z_gt);
	natural_destroy(y);
	natural_destroy(x);
}

void natural_divide_test(const size_t i)
{
	natural_t *x, *y, *z_gt, *z_p;
	if (i == 0)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("382347902387403287431784038749031874503874392847");
		z_gt = natural_from_string("1");
	}
	else if (i == 1)
	{
		x = natural_from_string("382347902387403287431784038749031874503874392847");
		y = natural_from_string("534436403405035981305983960138503953050319850319");
		z_gt = natural_from_string("0");
	}
	else if (i == 2)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("1");
	}
	else if (i == 3)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("1");
		z_gt = natural_from_string("652421309814098120348912049830582304982304921309");
	}
	else if (i == 4)
	{
		x = natural_from_string("0");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("0");
	}
	z_p = natural_from_uint(0);
	natural_divide(x, y, z_p);
	assert(natural_compare(z_gt, z_p) == EQUAL);
	natural_destroy(z_p);
	natural_destroy(z_gt);
	natural_destroy(y);
	natural_destroy(x);
}

void natural_modulo_test(const size_t i)
{
	natural_t *x, *y, *z_gt, *z_p;
	if (i == 0)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("382347902387403287431784038749031874503874392847");
		z_gt = natural_from_string("152088501017632693874199921389472078546445457472");
	}
	else if (i == 1)
	{
		x = natural_from_string("382347902387403287431784038749031874503874392847");
		y = natural_from_string("534436403405035981305983960138503953050319850319");
		z_gt = natural_from_string("382347902387403287431784038749031874503874392847");
	}
	else if (i == 2)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("0");
	}
	else if (i == 3)
	{
		x = natural_from_string("652421309814098120348912049830582304982304921309");
		y = natural_from_string("1");
		z_gt = natural_from_string("0");
	}
	else if (i == 4)
	{
		x = natural_from_string("0");
		y = natural_from_string("652421309814098120348912049830582304982304921309");
		z_gt = natural_from_string("0");
	}
	z_p = natural_from_uint(0);
	natural_modulo(x, y, z_p);
	assert(natural_compare(z_gt, z_p) == EQUAL);
	natural_destroy(z_p);
	natural_destroy(z_gt);
	natural_destroy(y);
	natural_destroy(x);
}

void natural_compare_test(const size_t i)
{
	natural_t *x, *y;
	comparison_t c_gt, c_p;
	if (i == 0)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("382347902387403287431784038749031874503874392847");
		c_gt = GREATER;
	}
	else if (i == 1)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("87403287431784038749031874503874392847");
		c_gt = GREATER;
	}
	else if (i == 2)
	{
		x = natural_from_string("534436403405035981305983960138503953050319850319");
		y = natural_from_string("534436403405035981305983960138503953050319850319");
		c_gt = EQUAL;
	}
	else if (i == 3)
	{
		x = natural_from_string("87403287431784038749031874503874392847");
		y = natural_from_string("534436403405035981305983960138503953050319850319");
		c_gt = LESS;
	}
	else if (i == 4)
	{
		x = natural_from_string("382347902387403287431784038749031874503874392847");
		y = natural_from_string("534436403405035981305983960138503953050319850319");
		c_gt = LESS;
	}
	c_p = natural_compare(x, y);
	assert(c_gt == c_p);
	natural_destroy(y);
	natural_destroy(x);
}

void natural_parity_test(const size_t i)
{
	natural_t *x;
	parity_t p_gt, p_p;
	if (i == 0)
	{
		x = natural_from_string("0");
		p_gt = EVEN;
	}
	else if (i == 1)
	{
		x = natural_from_string("1");
		p_gt = ODD;
	}
	p_p = natural_parity(x);
	assert(p_gt == p_p);
	natural_destroy(x);
}

int main()
{
	for (size_t i = 0; i < 2; ++i)
		natural_uint_test(i);
	for (size_t i = 0; i < 3; ++i)
		natural_string_test(i);
	for (size_t i = 0; i < 3; ++i)
		natural_add_test(i);
	for (size_t i = 0; i < 3; ++i)
		natural_subtract_test(i);
	for (size_t i = 0; i < 5; ++i)
		natural_multiply_test(i);
	for (size_t i = 0; i < 5; ++i)
		natural_divide_test(i);
	for (size_t i = 0; i < 5; ++i)
		natural_modulo_test(i);
	for (size_t i = 0; i < 5; ++i)
		natural_compare_test(i);
	for (size_t i = 0; i < 2; ++i)
		natural_parity_test(i);
	exit(EXIT_SUCCESS);
}
