#include <assert.h>
#include <stdlib.h>
#include <rsa/rsa.h>

void rsa_test(const size_t bits)
{
	rsa_key_t *public_key, *private_key;
	public_key = rsa_key_create();
	private_key = rsa_key_create();

	natural_t *input_gt, *output_p, *input_p;
	input_gt = natural_random(bits/2);
	output_p = natural_from_uint(0);
	input_p = natural_from_uint(0);

	rsa_generate_keys(bits, public_key, private_key);
	rsa_transform(input_gt, public_key, output_p);
	rsa_transform(output_p, private_key, input_p);
	assert(natural_compare(input_gt, input_p) == EQUAL);

	natural_destroy(input_p);
	natural_destroy(output_p);
	natural_destroy(input_gt);

	rsa_key_destroy(private_key);
	rsa_key_destroy(public_key);
}

int main()
{
	for (size_t i = 1; i < 4; ++i)
		rsa_test(256*i);
	exit(EXIT_SUCCESS);
}
