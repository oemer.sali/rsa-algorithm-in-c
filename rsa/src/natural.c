/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <rsa/natural.h>

void _natural_divide_modulo(const natural_t *x, const natural_t *y, natural_t *z, natural_t *w)
{
	if (x->size < y->size)
	{
		z->size = 0;
		free(z->digits);
		z->digits = NULL;

		w->size = x->size;
		if (x->size == 0)
		{
			free(w->digits);
			w->digits = NULL;
		}
		else
		{
			w->digits = realloc(w->digits, x->size*sizeof(digit_t));
			for (size_t i = 0; i < x->size; ++i)
				w->digits[i] = x->digits[i];
		}
	}
	else
	{
		natural_t q;
		q.size = x->size-y->size+1;
		q.digits = malloc(q.size*sizeof(digit_t));

		natural_t r;
		r.size = x->size+1;
		r.digits = malloc(r.size*sizeof(digit_t));
		for (size_t i = 0; i < x->size; ++i)
			r.digits[i] = x->digits[i];
		r.digits[x->size] = 0;

		digit_t *diff = malloc(r.size*sizeof(digit_t));
		size_t i = q.size;
		while (i > 0)
		{
			--i;
			q.digits[i] = 0;
			size_t j = 8*sizeof(digit_t);
			while (j > 0)
			{
				--j;
				digit_t k = i;
				bool borrow_in = false;
				for (size_t l = 0; l <= y->size; ++k, ++l)
				{
					digit_t shifted_block = 0;
					if (l > 0 && j > 0)
					 	shifted_block |= (y->digits[l-1] >> (8*sizeof(digit_t)-j));
					if (l < y->size)
						shifted_block |= (y->digits[l] << j);
					diff[k] = r.digits[k]-shifted_block;
					bool borrow_out = (r.digits[k] < diff[k]);
					if (borrow_in)
					{
						borrow_out |= (diff[k] == 0);
						--diff[k];
					}
					borrow_in = borrow_out;
				}
				for (; k < x->size && borrow_in; ++k)
				{
					borrow_in = (r.digits[k] == 0);
					diff[k] = r.digits[k]-1;
				}
				if (!borrow_in)
				{
					q.digits[i] |= ((digit_t)1<<j);
					while (k > i)
					{
						--k;
						r.digits[k] = diff[k];
					}
				}
			}
		}
		free(diff);

		if (q.digits[q.size-1] == 0)
		{
			--q.size;
			if (q.size == 0)
			{
				free(q.digits);
				q.digits = NULL;
			}
			else
				q.digits = realloc(q.digits, q.size*sizeof(digit_t));
		}
		z->size = q.size;
		free(z->digits);
		z->digits = q.digits;

		while (r.size > 0 && r.digits[r.size-1] == 0)
			--r.size;
		if (r.size <= x->size)
		{
			if (r.size == 0)
			{
				free(r.digits);
				r.digits = NULL;
			}
			else
				r.digits = realloc(r.digits, r.size*sizeof(digit_t));
		}
		w->size = r.size;
		free(w->digits);
		w->digits = r.digits;
	}
}

natural_t *natural_from_uint(const uint_t n)
{
	natural_t *x = malloc(sizeof(natural_t));
	if (n == 0)
	{
		x->size = 0;
		x->digits = NULL;
	}
	else
	{
		x->size = 1;
		x->digits = malloc(sizeof(digit_t));
		x->digits[0] = n;
	}
	return x;
}

uint_t natural_to_uint(const natural_t *x)
{
	if (x->size == 0)
	  	return 0;
	else
		return x->digits[0];
}

natural_t *natural_from_string(const char *s)
{
	natural_t *x = natural_from_uint(0);
	natural_t *base = natural_from_uint(10);
	for (size_t i = 0; s[i] != '\0'; ++i)
	{
		natural_multiply(x, base, x);
		if (s[i] != '0')
		{
			natural_t *digit = natural_from_uint(s[i]-'0');
			natural_add(x, digit, x);
			natural_destroy(digit);
		}
	}
	natural_destroy(base);
	return x;
}

char *natural_to_string(const natural_t *x)
{
	size_t n = 0;
	char *s = malloc((n+1)*sizeof(char));
	natural_t *base = natural_from_uint(10);
	natural_t *digit = natural_from_uint(0);
	natural_t *y = natural_from_uint(0);
	natural_assign(x, y);
	do
	{
		++n;
		s = realloc(s, (n+1)*sizeof(char));
		_natural_divide_modulo(y, base, y, digit);
		s[n-1] = natural_to_uint(digit)+'0';
	}
	while (y->size != 0);
	natural_destroy(y);
	natural_destroy(digit);
	natural_destroy(base);
	for (size_t i = 0; i < n/2; ++i)
	{
		s[n] = s[i];
		s[i] = s[n-i-1];
		s[n-i-1] = s[n];
	}
	s[n] = '\0';
	return s;
}

natural_t *natural_random(const size_t b)
{
	natural_t *x = malloc(sizeof(natural_t));
	if (b == 0)
	{
		x->size = 0;
		x->digits = NULL;
	}
	else
	{
		const size_t width = 8*sizeof(digit_t);
		x->size = (b+width-1)/width;
		x->digits = malloc(x->size*sizeof(digit_t));
		for (size_t i = 0; i < x->size; ++i)
		{
			x->digits[i] = 0;
			for (size_t j = 0; j < sizeof(digit_t)/2; ++j)
			{
				x->digits[i] <<= 16;
				x->digits[i] |= (uint16_t)rand()<<1;
				x->digits[i] ^= (uint16_t)rand()&1;
			}
		}
		const size_t shift = width-b%width;
		if (shift < width)
			x->digits[x->size-1] >>= shift;
		while (x->size > 0 && x->digits[x->size-1] == 0)
			--x->size;
		if (x->size == 0)
		{
			free(x->digits);
			x->digits = NULL;
		}
		else
			x->digits = realloc(x->digits, x->size*sizeof(digit_t));
	}
	return x;
}

void natural_destroy(natural_t *x)
{
	free(x->digits);
	free(x);
}

void natural_assign(const natural_t *x, natural_t *y)
{
	y->size = x->size;
	if (x->size == 0)
	{
		free(y->digits);
		y->digits = NULL;
	}
	else
	{
		y->digits = realloc(y->digits, x->size*sizeof(digit_t));
		for (size_t i = 0; i < x->size; ++i)
			y->digits[i] = x->digits[i];
	}
}

void natural_add(const natural_t *x, const natural_t *y, natural_t *z)
{
	if (x->size > y->size)
	{
		const natural_t *t = x;
		x = y;
		y = t;
	}
	if (y->size == 0)
	{
		z->size = 0;
		free(z->digits);
		z->digits = NULL;
	}
	else
	{
		natural_t s;
		s.size = y->size;
		s.digits = malloc(s.size*sizeof(digit_t));
		size_t i = 0;
		bool carry_in = false;
		for (; i < x->size; ++i)
		{
			s.digits[i] = x->digits[i]+y->digits[i];
			bool carry_out = (s.digits[i] < y->digits[i]);
			if (carry_in)
			{
				++s.digits[i];
				carry_out |= (s.digits[i] == 0);
			}
			carry_in = carry_out;
		}
		for (; i < y->size && carry_in; ++i)
		{
			s.digits[i] = y->digits[i]+1;
			carry_in = (s.digits[i] == 0);
		}
		for (; i < y->size; ++i)
			s.digits[i] = y->digits[i];
		if (carry_in)
		{
			++s.size;
			s.digits = realloc(s.digits, s.size*sizeof(digit_t));
			s.digits[i] = 1;
		}
		z->size = s.size;
		free(z->digits);
		z->digits = s.digits;
	}
}

void natural_subtract(const natural_t *x, const natural_t *y, natural_t *z)
{
	if (x->size == 0)
	{
		z->size = 0;
		free(z->digits);
		z->digits = NULL;
	}
	else
	{
		natural_t d;
		d.size = x->size;
		d.digits = malloc(d.size*sizeof(digit_t));
		size_t i = 0;
		bool borrow_in = false;
		for (; i < y->size; ++i)
		{
			d.digits[i] = x->digits[i]-y->digits[i];
			bool borrow_out = (d.digits[i] > x->digits[i]);
			if (borrow_in)
			{
				borrow_out |= (d.digits[i] == 0);
				--d.digits[i];
			}
			borrow_in = borrow_out;
		}
		for (; borrow_in; ++i)
		{
			borrow_in = (x->digits[i] == 0);
			d.digits[i] = x->digits[i]-1;
		}
		for (; i < x->size; ++i)
			d.digits[i] = x->digits[i];
		while (d.size > 0 && d.digits[d.size-1] == 0)
			--d.size;
		if (d.size < x->size)
		{
			if (d.size == 0)
			{
				free(d.digits);
				d.digits = NULL;
			}
			else
				d.digits = realloc(d.digits, d.size*sizeof(digit_t));
		}
		z->size = d.size;
		free(z->digits);
		z->digits = d.digits;
	}
}

void natural_multiply(const natural_t *x, const natural_t *y, natural_t *z)
{
	if (x->size == 0 || y->size == 0)
	{
		z->size = 0;
		free(z->digits);
		z->digits = NULL;
	}
	else
	{
		if (x->size > y->size)
		{
			const natural_t *t = x;
			x = y;
			y = t;
		}
		natural_t p;
		p.size = x->size+y->size;
		p.digits = malloc(p.size*sizeof(digit_t));
		for (size_t i = 0; i < p.size; ++i)
			p.digits[i] = 0;
		for (size_t i = 0; i < x->size; ++i)
			if (x->digits[i] != 0)
				for (size_t j = 0; j < 8*sizeof(digit_t); ++j)
					if ((x->digits[i]&((digit_t)1<<j)) != 0)
					{
						size_t k = i;
						bool carry_in = false;
						for (size_t l = 0; l <= y->size; ++k, ++l)
						{
							digit_t shifted_block = 0;
							if (l > 0 && j > 0)
							 	shifted_block |= (y->digits[l-1] >> (8*sizeof(digit_t)-j));
							if (l < y->size)
								shifted_block |= (y->digits[l] << j);
							p.digits[k] += shifted_block;
							bool carry_out = (p.digits[k] < shifted_block);
							if (carry_in)
							{
								++p.digits[k];
								carry_out |= (p.digits[k] == 0);
							}
							carry_in = carry_out;
						}
						for (; carry_in; ++k)
						{
							++p.digits[k];
							carry_in = (p.digits[k] == 0);
						}
					}
		if (p.digits[p.size-1] == 0)
		{
			--p.size;
			p.digits = realloc(p.digits, p.size*sizeof(digit_t));
		}
		z->size = p.size;
		free(z->digits);
		z->digits = p.digits;
	}
}

void natural_divide(const natural_t *x, const natural_t *y, natural_t *z)
{
	natural_t *w = natural_from_uint(0);
	_natural_divide_modulo(x, y, z, w);
	natural_destroy(w);
}

void natural_modulo(const natural_t *x, const natural_t *y, natural_t *z)
{
	natural_t *w = natural_from_uint(0);
	_natural_divide_modulo(x, y, w, z);
	natural_destroy(w);
}

comparison_t natural_compare(const natural_t *x, const natural_t *y)
{
	if (x->size < y->size)
		return LESS;
	else if (x->size > y->size)
		return GREATER;
	else
	{
		for (size_t i = x->size; i > 0; --i)
			if (x->digits[i-1] < y->digits[i-1])
				return LESS;
			else if (x->digits[i-1] > y->digits[i-1])
				return GREATER;
		return EQUAL;
	}
}

parity_t natural_parity(const natural_t *x)
{
	if (x->size == 0 || x->digits[0]%2 == 0)
		return EVEN;
	else
		return ODD;
}
