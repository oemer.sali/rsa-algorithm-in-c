/**
 * Copyright (C) 2019  Ömer Sali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <time.h>
#include <rsa/natural.h>
#include <rsa/rsa.h>

void _rsa_modular_exponentiation(const natural_t *b, const natural_t *e, const natural_t *m, natural_t *r)
{
	natural_t *zero, *one, *two;
	zero = natural_from_uint(0);
	one = natural_from_uint(1);
	two = natural_from_uint(2);

	if (natural_compare(m, one) == EQUAL)
		natural_assign(zero, r);
	else
	{
		natural_t *b0, *e0;
		b0 = natural_from_uint(0);
		e0 = natural_from_uint(0);
		natural_assign(b, b0);
		natural_assign(e, e0);

		natural_assign(one, r);
		natural_modulo(b0, m, b0);
		while (natural_compare(e0, zero) == GREATER)
		{
			if (natural_parity(e0) == ODD)
			{
				natural_multiply(r, b0, r);
				natural_modulo(r, m, r);
			}
			natural_divide(e0, two, e0);
			natural_multiply(b0, b0, b0);
			natural_modulo(b0, m, b0);
		}

		natural_destroy(e0);
		natural_destroy(b0);
	}

	natural_destroy(two);
	natural_destroy(one);
	natural_destroy(zero);
}

bool _rsa_modular_inverse(const natural_t *e, const natural_t *n, natural_t *d)
{
	natural_t *zero, *one;
	zero = natural_from_uint(0);
	one = natural_from_uint(1);

	natural_t *d0, *d1;
	d0 = natural_from_uint(0);
	d1 = natural_from_uint(1);

	natural_t *e0, *e1;
	e0 = natural_from_uint(0);
	natural_assign(n, e0);
	e1 = natural_from_uint(0);
	natural_assign(e, e1);

	bool sign = true;
	natural_t *q = natural_from_uint(0);
	natural_t *t = natural_from_uint(0);

	while (natural_compare(e1, zero) != EQUAL)
	{
		sign = !sign;
		natural_divide(e0, e1, q);

		natural_assign(d0, t);
		natural_assign(d1, d0);
		natural_multiply(q, d1, d1);
		natural_add(t, d1, d1);

		natural_assign(e0, t);
		natural_assign(e1, e0);
		natural_multiply(q, e1, e1);
		natural_subtract(t, e1, e1);
	}

	natural_destroy(t);
	natural_destroy(q);

	bool is_unit = natural_compare(e0, one) == EQUAL;
	if (sign && natural_compare(d0, zero) != EQUAL)
		natural_subtract(n, d0, d);
	else
		natural_assign(d0, d);

	natural_destroy(e1);
	natural_destroy(e0);
	natural_destroy(d1);
	natural_destroy(d0);
	natural_destroy(one);
	natural_destroy(zero);
	return is_unit;
}

bool _rsa_miller_rabin_test(const natural_t *n, const natural_t *a)
{
	bool is_prime = false;
	natural_t *zero, *one, *two;
	zero = natural_from_uint(0);
	one = natural_from_uint(1);
	two = natural_from_uint(2);

	// Calculate n-1 = d*2^j
	natural_t *d = natural_from_uint(0);
	natural_subtract(n, one, d);
	size_t j = 0;
	while (natural_parity(d) == EVEN)
	{
		natural_divide(d, two, d);
		++j;
	}

	// Perform test for given basis a
	natural_t *r = natural_from_uint(0);
	_rsa_modular_exponentiation(a, d, n, r);
	if (natural_compare(r, one) == EQUAL)
		is_prime = true;
	else
	{
		natural_t *m = natural_from_uint(0);
		natural_subtract(n, one, m);
		for (size_t i = 0; i < j && !is_prime; ++i)
		{
			if (natural_compare(r, m) == EQUAL)
				is_prime = true;
			else
				_rsa_modular_exponentiation(r, two, n, r);
		}
		natural_destroy(m);
	}
	natural_destroy(r);
	natural_destroy(d);

	natural_destroy(two);
	natural_destroy(one);
	natural_destroy(zero);
	return is_prime;
}

natural_t *_rsa_choose_prime(size_t b, size_t k)
{
	// Calculate Riemann bound for witnesses a
	const size_t bound = ceil(log2(floor(2*pow(log(2)*b, 2))));

	natural_t *zero, *one, *three;
	zero = natural_from_uint(0);
	one = natural_from_uint(1);
	three = natural_from_uint(3);
	while (true)
	{
		// Choose odd prime candidate p > 3
		natural_t *p = natural_from_uint(0);
		do
		{
			natural_destroy(p);
			p = natural_random(b);
		}
		while (natural_parity(p) == EVEN
			|| natural_compare(p, three) != GREATER);

		// Perform k iterations with randomly chosen witnesses
		bool is_prime = true;
		natural_t *q = natural_from_uint(0);
		natural_subtract(p, one, q);
		natural_t *a = natural_from_uint(0);
		for (size_t i = 0; i < k && is_prime; ++i)
		{
			do
			{
				natural_destroy(a);
				a = natural_random(bound);
			}
			while (natural_compare(a, one) != GREATER
				|| natural_compare(a, q) != LESS);
			is_prime &= _rsa_miller_rabin_test(p, a);
		}
		natural_destroy(a);
		natural_destroy(q);
		if (is_prime)
		{
			natural_destroy(three);
			natural_destroy(one);
			natural_destroy(zero);
			return p;
		}
		else
			natural_destroy(p);
	}
}

rsa_key_t *rsa_key_create()
{
	rsa_key_t *key = malloc(sizeof(rsa_key_t));
	key->exponent = natural_from_uint(0);
	key->modulus = natural_from_uint(0);
	return key;
}

void rsa_key_destroy(rsa_key_t *key)
{
	natural_destroy(key->exponent);
	natural_destroy(key->modulus);
	free(key);
}

void rsa_generate_keys(const size_t bits, rsa_key_t *public_key, rsa_key_t *private_key)
{
	srand(time(NULL));
	natural_t *one = natural_from_uint(1);

	// Choose prime numbers p, q
	natural_t *p, *q;
	p = _rsa_choose_prime(bits/2, 16);
	q = _rsa_choose_prime((bits+1)/2, 16);

	// Calculate RSA-modulus n
	natural_t *n;
	n = natural_from_uint(0);
	natural_multiply(p, q, n);

	// Calculate Euler's totient function phi(n)
	natural_t *phi_n;
	phi_n = natural_from_uint(0);
	natural_subtract(p, one, p);
	natural_subtract(q, one, q);
	natural_multiply(p, q, phi_n);

	// Calculate exponents d, e with d*e=1 (mod phi(n))
	natural_t *d, *e;
	d = natural_from_uint(0);
	e = natural_from_uint(0);
	do
	{
		natural_destroy(e);
		e = natural_random(bits/2);
	}
	while (natural_compare(e, one) != GREATER
		|| natural_compare(e, phi_n) != LESS
		|| !_rsa_modular_inverse(e, phi_n, d));

	// Save public and private keys
	natural_assign(e, public_key->exponent);
	natural_assign(n, public_key->modulus);
	natural_assign(d, private_key->exponent);
	natural_assign(n, private_key->modulus);

	natural_destroy(e);
	natural_destroy(d);
	natural_destroy(phi_n);
	natural_destroy(n);
	natural_destroy(p);
	natural_destroy(q);
	natural_destroy(one);
}

void rsa_transform(const natural_t *input, const rsa_key_t *key, natural_t *output)
{
	_rsa_modular_exponentiation(input, key->exponent, key->modulus, output);
}
